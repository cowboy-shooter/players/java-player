# Dummy Java Player

## Start

```bash
mvn clean install
```
```bash
java -jar target/java-player-1.0-SNAPSHOT.jar
```

Server is callable on http://localhost:8080/my-player/move
post example:

```json
{
    "sequenceNumber": 1,
    "opponentsLastMove": "RELOAD",
    "opponentId": "1234",
    "opponentName": "Muj tym"
}
```


## What it does

In `com.cowboy.shooter.dummyplayer.service.GameService.makeMove(MatchMove lastMove)` is basic logic
 * Log Input
 * Pick random value
 * Send it