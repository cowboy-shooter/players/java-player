package com.cowboy.shooter.dummyplayer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyPlayerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyPlayerApplication.class, args);
	}

}
