package com.cowboy.shooter.dummyplayer.rest;

import com.cowboy.shooter.dummyplayer.rest.dto.MatchMove;
import com.cowboy.shooter.dummyplayer.rest.dto.TeamMoveRestResponse;
import com.cowboy.shooter.dummyplayer.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class GameRestController {

    @Autowired
    private GameService gameService;

    @PostMapping(path= "/move", consumes = "application/json", produces = "application/json")
    public TeamMoveRestResponse makeMove(@RequestBody MatchMove lastMove) {
        return gameService.makeMove(lastMove);
    }

}
