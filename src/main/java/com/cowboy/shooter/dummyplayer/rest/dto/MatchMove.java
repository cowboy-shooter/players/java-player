package com.cowboy.shooter.dummyplayer.rest.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MatchMove implements Serializable {

    private Integer sequenceNumber;
    private TeamMove opponentsLastMove;
    private String opponentId;
    private String opponentName;

}
