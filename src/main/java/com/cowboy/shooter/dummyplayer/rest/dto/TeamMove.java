package com.cowboy.shooter.dummyplayer.rest.dto;

public enum TeamMove {
    BLOCK,
    SHOOT,
    EVADE,
    RELOAD,
    NULL
}
