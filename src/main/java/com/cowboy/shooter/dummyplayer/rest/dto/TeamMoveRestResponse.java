package com.cowboy.shooter.dummyplayer.rest.dto;

import lombok.Data;

@Data
public class TeamMoveRestResponse {

    private TeamMove move;

    public TeamMoveRestResponse(TeamMove move) {
        this.move = move;
    }
}
