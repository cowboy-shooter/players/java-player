package com.cowboy.shooter.dummyplayer.service;

import com.cowboy.shooter.dummyplayer.rest.dto.MatchMove;
import com.cowboy.shooter.dummyplayer.rest.dto.TeamMove;
import com.cowboy.shooter.dummyplayer.rest.dto.TeamMoveRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;


@Service
public class GameService {

    Logger logger = LoggerFactory.getLogger(GameService.class);

    public TeamMoveRestResponse makeMove(MatchMove lastMove) {
        // Your logic goes here
        logger.info("Opponents last move was: " + lastMove);
        TeamMove[] possibleValues = TeamMove.values();
        return new TeamMoveRestResponse(possibleValues[new Random().nextInt(possibleValues.length)]);
    }
}
